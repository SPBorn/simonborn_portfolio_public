# PSO

Particle Swarm Optimization (PSO) is a population-based optimization algorithm inspired by the social behavior of birds flocking or fish schooling. It was developed by James Kennedy and Russell Eberhart in 1995. PSO is used to solve optimization problems by iteratively trying to improve candidate solutions with respect to a given measure of quality or "fitness.

## Loop dependencies

### global best
The updates to the global best (gBestValue, gBestPosition) need to be protected from race conditions since multiple particles could find a new best value at the same time.

'''cpp{

    // Global & Personal Bests
    for (int p = 0; p < Np; p++) {
        if (M[p] > gBestValue) {
            gBestValue = M[p];
            for (int i = 0; i < Nd; i++) {
                gBestPosition[i] = R[p][i];
            }
            bestTimeStep = j;
        }

        // Personal Best Update
        if (M[p] > pBestValue[p]) {
            pBestValue[p] = M[p];
            for (int i = 0; i < Nd; i++) {
                pBestPosition[p][i] = R[p][i];
            }
        }
    }
    
}
### numEvals
The variable numEvals is incremented in each iteration of the loop by different particles. Without synchronization, this could result in race conditions where the value of numEvals is incorrectly updated.

'''cpp{

    // Evaluate Fitness
    for (int p = 0; p < Np; p++) {
        M[p] = objFunc(R, Nd, p);
        numEvals++;
    }

}

### gBest position
This loop updates each particle's velocity, and in doing so, it accesses the global best position (gBestPosition). Though each particle's velocity is independent, the access to gBestPosition introduces a dependency.

'''cpp{

    // Update Velocities
    for (int p = 0; p < Np; p++) {
        for (int i = 0; i < Nd; i++) {
            R1 = randDbl(0, 1);
            R2 = randDbl(0, 1);
            V[p][i] = w * V[p][i] + C1 * R1 * (pBestPosition[p][i] - R[p][i]) + C2 * R2 * (gBestPosition[i] - R[p][i]);
            if (V[p][i] > vMax) { V[p][i] = randDbl(vMin, vMax); }
            if (V[p][i] < vMin) { V[p][i] = randDbl(vMin, vMax); }
        }
    }

}


## Loop Scheduling

For all the parallelized loops, I used #pragma omp parallel for for distributing iterations across multiple threads. By default, OpenMP uses static scheduling, which works well for this case because each iteration (each particle's position, velocity, or fitness update) takes approximately the same amount of time.

### Some Deeper Analysis

Since each particle in the swarm operates independently (except for the updates to the global best), it was straightforward to parallelize the fitness, velocity, and position updates. These operations can be efficiently divided across multiple threads.

<br/>

Updating the global best (gBestValue, gBestPosition) could result in race conditions if multiple threads update it simultaneously. A critical section was necessary to protect these updates. I chose this design to ensure correctness while keeping the code simple.

<br/>

The numEvals variable (tracking the number of evaluations) is updated in the fitness evaluation loop. Without synchronization, multiple threads could try to increment numEvals at the same time, causing data corruption. An atomic directive was used to ensure that increments are thread-safe.

### Some Issues

The main issues I encountered were with finding where to synchronize the data and threads. After finding several loop dependencies I dropped a couple #pragma sections to start the parralelization process where there would not be a problem. The fitness evaluation made enough sense as it was simply one line that was causing issues with the result, so making that atomic would fix the problem without adding too much overhead. It took a while for me to realize what was wrong with parralelizing the global update.


## Performance Analysis

### Speedup
![Speedup](Results/speedup_threads.png) <br>
The speedup curve shows how much faster the parallel version of the algorithm runs compared to the serial version. As the number of threads increases, we see the speedup increase, indicating better performance. However, the curve may flatten out at higher thread counts, indicating diminishing returns due to overhead in managing more threads

### Efficiency
![Efficiency](Results/eff_threads.png) <br>
Efficiency measures how well the parallel system scales with the number of threads. We can see that efficiency decreases as the thread count increases, which is typical because the overhead of coordinating threads grows, and the system becomes less efficient. Perfect efficiency would be 1 (100%), but this drops as more threads are added.

### Karp-Flatt
![Karp-Flatt](Results/Karp_flat_threads.png) <br>
The Karp-Flatt metric helps identify the proportion of time spent in parallelizable vs non-parallelizable portions of the computation. As the number of threads increases, the metric reflects the fraction of the computation that is inherently serial. A lower Karp-Flatt value indicates better parallelization. The plot reveals whether the algorithm scales efficiently with more threads or if there are bottlenecks in serial portions of the code.


## How to Run

-g++ -fopenmp main.cpp CStopWatch.cpp -o pso.exe <br>
-./pso.exe > results.csv <br>



## How to run at OSC
-sbatch jobScript.slurm