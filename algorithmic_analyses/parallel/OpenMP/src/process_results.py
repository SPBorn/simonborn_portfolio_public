import matplotlib
matplotlib.use('Agg')
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

def csv_to_markdown(csv_file):
    # Read the CSV file using pandas
    df = pd.read_csv(csv_file)
    
    # Initialize the Markdown table as a string
    markdown_table = ""
    
    # Create the header row
    headers = df.columns.tolist()
    header_row = "| " + " | ".join(headers) + " |"
    separator_row = "| " + " | ".join(["---"] * len(headers)) + " |"
    
    # Add header and separator to the markdown table
    markdown_table += header_row + "\n" + separator_row + "\n"
    
    # Add data rows
    for index, row in df.iterrows():
        row_data = "| " + " | ".join(str(value) for value in row) + " |"
        markdown_table += row_data + "\n"
    
    return markdown_table

def create_plots(input_csv):
    # Read the CSV file
    
    output_csv_file = 'metrics_output.csv'

    # Read the data into a DataFrame
    data = pd.read_csv(input_csv)

    # Calculate Speedup, Efficiency, and Karp-Flatt
    # Using the time taken with 1 thread as the baseline for speedup calculations
    baseline_time = data[data['Thread Count'] == 1]['Total Time'].values[0]

    data['Speedup'] = baseline_time / data['Total Time']
    data['Efficiency'] = data['Speedup'] / data['Thread Count']
    data['Karp-Flatt'] = 2.0 / data['Efficiency']

    # Save the new metrics to a new CSV file
    data.to_csv(output_csv_file, index=False)

    # Plotting the metrics
    # Speedup Plot
    plt.figure(figsize=(10, 5))
    plt.plot(data['Thread Count'], data['Speedup'], marker='o', color='b', label='Speedup')
    plt.title('Speedup vs Thread Count')
    plt.xlabel('Thread Count')
    plt.ylabel('Speedup')
    plt.grid()
    plt.legend()
    plt.savefig('speedup_vs_thread_count.png')
    plt.close()  # Close the figure to avoid overlapping in future plots

    # Efficiency Plot
    plt.figure(figsize=(10, 5))
    plt.plot(data['Thread Count'], data['Efficiency'], marker='o', color='g', label='Efficiency')
    plt.title('Efficiency vs Thread Count')
    plt.xlabel('Thread Count')
    plt.ylabel('Efficiency')
    plt.grid()
    plt.legend()
    plt.savefig('efficiency_vs_thread_count.png')
    plt.close()  # Close the figure

    # Karp-Flatt Plot
    plt.figure(figsize=(10, 5))
    plt.plot(data['Thread Count'], data['Karp-Flatt'], marker='o', color='r', label='Karp-Flatt')
    plt.title('Karp-Flatt vs Thread Count')
    plt.xlabel('Thread Count')
    plt.ylabel('Karp-Flatt')
    plt.grid()
    plt.legend()
    plt.savefig('karp_flatt_vs_thread_count.png')
    plt.close()  # Close the figure

    print(csv_to_markdown(output_csv_file))

create_plots("output.csv")
