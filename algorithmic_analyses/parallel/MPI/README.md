# Parallel Particle Swarm Optimization (PSO) using MPI

## Algorithm Overview
Particle Swarm Optimization (PSO) is a population-based optimization algorithm inspired by the social behavior of birds flocking or fish schooling. In this implementation, we parallelized PSO using Message Passing Interface (MPI) to improve performance on the Rastrigin benchmark function.

### Key Components of PSO:
- **Particles**: Each particle represents a candidate solution
- **Position**: Current solution in the search space
- **Velocity**: Direction and magnitude of movement
- **Personal Best**: Best solution found by individual particle
- **Global Best**: Best solution found by entire swarm

### Parallelization Strategy
The implementation uses a domain decomposition approach:

1. **Population Distribution**: 
   - Total population divided among available processors
   - Each processor handles a subset of particles (`localNp`)

2. **Parallel Components**:
   - **Local Updates**: Position and velocity updates performed independently
   - **Fitness Evaluation**: Each processor evaluates its local particles
   - **Global Communication**: MPI_Allreduce for global best updates
   - **Synchronization**: Global best position broadcast to all processors

## Performance Analysis

### Benchmark Results

| Processors | Time (s) | Speedup | Efficiency | Karp-Flatt |
|------------|----------|---------|------------|------------|
| 1          | 10.70    | 1.27    | 1.27      | -inf       |
| 2          | 5.38     | 2.53    | 1.26      | 0.395      |
| 3          | 3.66     | 3.71    | 1.24      | 0.269      |
| 4          | 2.71     | 5.03    | 1.26      | 0.199      |
| 5          | 2.18     | 6.23    | 1.25      | 0.160      |
| 6          | 1.87     | 7.28    | 1.21      | 0.137      |
| 7          | 1.65     | 8.25    | 1.18      | 0.121      |
| 8          | 1.43     | 9.51    | 1.19      | 0.105      |
| 9          | 1.33     | 10.25   | 1.14      | 0.098      |
| 10         | 1.13     | 12.03   | 1.20      | 0.083      |

### Performance Metrics Analysis

1. **Speedup Analysis**
   - Near-linear speedup up to 10 processors
   - Maximum speedup of 12.03x with 10 processors
   - Consistently good scaling behavior

2. **Efficiency Analysis**
   - Maintains high efficiency (>1.14) across all processor counts
   - Slight decrease as processor count increases
   - Super-linear efficiency observed due to cache effects

3. **Karp-Flatt Metric**
   - Decreasing trend indicates good parallelization
   - Low values at higher processor counts suggest minimal serial fraction
   - Validates scalability of implementation

## Performance Visualizations

### Speedup Plot
![Speedup vs Number of Processors](./Results/speedup-graph.svg)


### Efficiency Plot
<img src="./Results/efficiency-graph.svg" alt="Efficiency vs Number of Processors" width="600"/>


### Karp-Flatt Plot
![Karp-Flatt Metric vs Number of Processors](./Results/karpflatt-graph.svg)


## Implementation Details

### Key Code Sections

1. **Domain Decomposition**
```cpp
int localNp = Np / size;
int remainder = Np % size;
if (rank < remainder) localNp++;
```

2. **Global Best Communication**
```cpp
struct {
    double value;
    int rank;
} in, out;
in.value = localBestFitness;
in.rank = rank;
MPI_Allreduce(&in, &out, 1, MPI_DOUBLE_INT, MPI_MAXLOC, MPI_COMM_WORLD);
```

## Conclusions

The MPI parallelization of PSO demonstrates excellent scalability:

1. **Scalability**: Near-linear speedup achieved
2. **Efficiency**: Maintained high efficiency across processor counts
3. **Communication Overhead**: Well-managed as shown by Karp-Flatt metric
4. **Load Balancing**: Effective distribution of particles

### Future Improvements
1. Implement hybrid MPI+OpenMP approach
2. Explore asynchronous communication patterns
3. Optimize memory access patterns
4. Investigate dynamic load balancing

## Build and Run Instructions

1. **Compilation**:
```bash
make OSC
```

2. **Execution**:
```bash
sbatch jobScript.slurm
```

3. **Results**:
Results are stored in `benchmark_results.csv`
