#include <mpi.h>
#include <iostream>
#include <vector>
#include <random>
#include "CStopWatch.h"

using namespace std;

typedef vector<double> ldArray1D;
typedef vector<ldArray1D> ldArray2D;

double randDbl(const double& min, const double& max) {
    static thread_local mt19937* generator = nullptr;
    if (!generator) generator = new mt19937(10);
    uniform_real_distribution<double> distribution(min, max);
    return distribution(*generator);
}

double Rastrigin(ldArray2D &R, int Nd, int p) {
    double Z = 0;
    for (int i = 0; i < Nd; i++) {
        double Xi = R[p][i];
        Z += (pow(Xi, 2) - 10 * cos(2 * M_PI * Xi) + 10);
    }
    return -Z;
}

void MPI_PSO(int Np, int Nd, int Nt, double xMin, double xMax, double vMin, double vMax,
             double (*objFunc)(ldArray2D &, int, int), int &numEvals, string functionName) {
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int localNp = Np / size;
    int remainder = Np % size;
    if (rank < remainder) localNp++;

    ldArray2D localR(localNp, vector<double>(Nd));
    ldArray2D localV(localNp, vector<double>(Nd));
    ldArray1D localFitness(localNp);
    ldArray2D localPBest(localNp, vector<double>(Nd));
    ldArray1D localPBestFitness(localNp);
    
    ldArray1D gBestPosition(Nd);
    double gBestFitness = -INFINITY;
    
    CStopWatch timer, timer1;
    double startTime = MPI_Wtime();
    float positionTime = 0, fitnessTime = 0, velocityTime = 0;
    int lastStep = Nt, bestTimeStep = 0;
    numEvals = 0;

    timer1.startTimer();

    // Initialize particles
    for (int i = 0; i < localNp; i++) {
        for (int j = 0; j < Nd; j++) {
            localR[i][j] = randDbl(xMin, xMax);
            localV[i][j] = randDbl(vMin, vMax);
        }
        localFitness[i] = objFunc(localR, Nd, i);
        localPBestFitness[i] = localFitness[i];
        localPBest[i] = localR[i];
        numEvals++;
    }

    double w = 0.9, wMin = 0.4, c1 = 1.45, c2 = 1.45;

    for (int t = 0; t < Nt; t++) {
        // Update positions
        timer.startTimer();
        w = 0.9 - ((0.9 - 0.4) * t) / Nt;

        for (int i = 0; i < localNp; i++) {
            for (int d = 0; d < Nd; d++) {
                double r1 = randDbl(0, 1);
                double r2 = randDbl(0, 1);
                localV[i][d] = w * localV[i][d] + 
                              c1 * r1 * (localPBest[i][d] - localR[i][d]) +
                              c2 * r2 * (gBestPosition[d] - localR[i][d]);
                
                localV[i][d] = min(max(localV[i][d], vMin), vMax);
                localR[i][d] += localV[i][d];
                localR[i][d] = min(max(localR[i][d], xMin), xMax);
            }
        }
        timer.stopTimer();
        positionTime += timer.getElapsedTime();

        // Evaluate fitness
        timer.startTimer();
        for (int i = 0; i < localNp; i++) {
            localFitness[i] = objFunc(localR, Nd, i);
            numEvals++;

            if (localFitness[i] > localPBestFitness[i]) {
                localPBestFitness[i] = localFitness[i];
                localPBest[i] = localR[i];
            }
        }

        double localBestFitness = -INFINITY;
        int localBestIndex = 0;
        for (int i = 0; i < localNp; i++) {
            if (localPBestFitness[i] > localBestFitness) {
                localBestFitness = localPBestFitness[i];
                localBestIndex = i;
            }
        }
        timer.stopTimer();
        fitnessTime += timer.getElapsedTime();

        // Update velocities and find global best
        timer.startTimer();
        struct {
            double value;
            int rank;
        } in, out;
        in.value = localBestFitness;
        in.rank = rank;
        
        MPI_Allreduce(&in, &out, 1, MPI_DOUBLE_INT, MPI_MAXLOC, MPI_COMM_WORLD);

        if (out.value > gBestFitness) {
            gBestFitness = out.value;
            bestTimeStep = t;
            if (rank == out.rank) {
                gBestPosition = localPBest[localBestIndex];
            }
            MPI_Bcast(gBestPosition.data(), Nd, MPI_DOUBLE, out.rank, MPI_COMM_WORLD);
        }
        timer.stopTimer();
        velocityTime += timer.getElapsedTime();

        if (gBestFitness >= -0.0001) {
            lastStep = t;
            break;
        }
    }

    timer1.stopTimer();
    double totalTime = timer1.getElapsedTime();
    double parallelTime = MPI_Wtime() - startTime;
    
    if (rank == 0) {

        double serialTime = 13.5995; // Set this from a separate serial run
        double speedup = serialTime / parallelTime;
        double efficiency = speedup / size;
        double e = (1/speedup - 1/size)/(1 - 1/size);
        
        cout << functionName    << ","
             << gBestFitness   << ","
             << Np             << ","
             << Nd             << ","
             << lastStep       << ","
             << bestTimeStep   << ","
             << numEvals       << ","
             << positionTime   << ","
             << fitnessTime    << ","
             << velocityTime   << ","
             << totalTime      << ","
             << size          << ","
             << parallelTime   << ","
             << speedup        << ","
             << efficiency     << ","
             << e             << endl;
    }
}

int main(int argc, char* argv[]) {
    MPI_Init(&argc, &argv);
    
    double (*rPtr)(ldArray2D &, int, int) = NULL;
    double xMin, xMax, vMin, vMax;
    int numEvals;

    rPtr = &Rastrigin;
    xMin = -5.12;
    xMax = 5.12;
    vMin = -1;
    vMax = 1;
    
    int Np = 100;
    int Nd = 100;
    int Nt = 10000;
    
    MPI_PSO(Np, Nd, Nt, xMin, xMax, vMin, vMax, rPtr, numEvals, "F2");
    
    MPI_Finalize();
    return 0;
}