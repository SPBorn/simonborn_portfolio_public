#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>
#include <unordered_map>
#include <iomanip>

using namespace std;
using namespace std::chrono;

// Recursive approach
long long fibRecursive(int n) {
    if (n <= 1) return n;
    return fibRecursive(n - 1) + fibRecursive(n - 2);
}

// Memoized approach
long long fibMemoized(int n, unordered_map<int, long long>& memo) {
    if (memo.find(n) != memo.end()) return memo[n];
    if (n <= 1) return n;
    
    memo[n] = fibMemoized(n - 1, memo) + fibMemoized(n - 2, memo);
    return memo[n];
}

// Bottom-up approach
long long fibBottomUp(int n) {
    if (n <= 1) return n;
    
    long long prev2 = 0;
    long long prev1 = 1;
    long long current = 0;
    
    for (int i = 2; i <= n; i++) {
        current = prev1 + prev2;
        prev2 = prev1;
        prev1 = current;
    }
    return current;
}

int main() {
    // Open input file
    ifstream inFile("fibonacci_input.txt");
    if (!inFile) {
        cerr << "Error opening input file" << endl;
        return 1;
    }

    // Open output CSV file
    ofstream outFile("fibonacci_results.csv");
    if (!outFile) {
        cerr << "Error opening output file" << endl;
        return 1;
    }

    // Write CSV header
    outFile << "Input Number,Method,Output,Time to Compute (ms)" << endl;

    int number;
    while (inFile >> number) {
        unordered_map<int, long long> memo;
        long long result;
        duration<double, milli> duration;
        // Test recursive approach
        auto start = high_resolution_clock::now();
        result = fibRecursive(number);
        auto end = high_resolution_clock::now();
        duration = end - start;
        outFile << number << ",Recursive," << result << "," 
                << fixed << setprecision(3) << duration.count() << endl;

        // Test memoized approach
        start = high_resolution_clock::now();
        result = fibMemoized(number, memo);
        end = high_resolution_clock::now();
        duration = end - start;
        outFile << number << ",Memoized," << result << "," 
                << fixed << setprecision(3) << duration.count() << endl;

        // Test bottom-up approach
        start = high_resolution_clock::now();
        result = fibBottomUp(number);
        end = high_resolution_clock::now();
        duration = end - start;
        outFile << number << ",Bottom-up," << result << "," 
                << fixed << setprecision(3) << duration.count() << endl;
    }

    inFile.close();
    outFile.close();
    
    cout << "Results have been written to fibonacci_results.csv" << endl;
    return 0;
}