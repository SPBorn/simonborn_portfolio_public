#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>
#include <climits>
#include <string>
#include <iomanip>
#include <sstream>

using namespace std;
using namespace std::chrono;

// Structure to store both minimum operations and parenthesization
struct MatrixChainResult {
    int operations;
    string parenthesization;
};

// Recursive approach
MatrixChainResult matrixChainRecursive(const vector<int>& dims, int i, int j) {
    if (i == j) {
        return {0, "M" + to_string(i)};
    }
    
    int minOps = INT_MAX;
    string bestParen;
    
    for (int k = i; k < j; k++) {
        MatrixChainResult left = matrixChainRecursive(dims, i, k);
        MatrixChainResult right = matrixChainRecursive(dims, k + 1, j);
        int operations = left.operations + right.operations + 
                        dims[i-1] * dims[k] * dims[j];
        
        if (operations < minOps) {
            minOps = operations;
            bestParen = "(" + left.parenthesization + right.parenthesization + ")";
        }
    }
    
    return {minOps, bestParen};
}

// Memoized approach
MatrixChainResult matrixChainMemoizedHelper(const vector<int>& dims, 
                                          vector<vector<MatrixChainResult>>& memo,
                                          int i, int j) {
    if (i == j) {
        return {0, "M" + to_string(i)};
    }
    
    if (memo[i][j].operations != -1) {
        return memo[i][j];
    }
    
    int minOps = INT_MAX;
    string bestParen;
    
    for (int k = i; k < j; k++) {
        MatrixChainResult left = matrixChainMemoizedHelper(dims, memo, i, k);
        MatrixChainResult right = matrixChainMemoizedHelper(dims, memo, k + 1, j);
        int operations = left.operations + right.operations + 
                        dims[i-1] * dims[k] * dims[j];
        
        if (operations < minOps) {
            minOps = operations;
            bestParen = "(" + left.parenthesization + right.parenthesization + ")";
        }
    }
    
    memo[i][j] = {minOps, bestParen};
    return memo[i][j];
}

MatrixChainResult matrixChainMemoized(const vector<int>& dims) {
    int n = dims.size() - 1;
    vector<vector<MatrixChainResult>> memo(n + 1, 
        vector<MatrixChainResult>(n + 1, {-1, ""}));
    return matrixChainMemoizedHelper(dims, memo, 1, n);
}

// Bottom-up approach
MatrixChainResult matrixChainBottomUp(const vector<int>& dims) {
    int n = dims.size() - 1;
    vector<vector<int>> m(n + 1, vector<int>(n + 1, 0));
    vector<vector<int>> split(n + 1, vector<int>(n + 1, 0));
    vector<vector<string>> parenthesization(n + 1, vector<string>(n + 1));
    
    // Initialize diagonal with matrix names
    for (int i = 1; i <= n; i++) {
        parenthesization[i][i] = "M" + to_string(i);
    }
    
    for (int len = 2; len <= n; len++) {
        for (int i = 1; i <= n - len + 1; i++) {
            int j = i + len - 1;
            m[i][j] = INT_MAX;
            
            for (int k = i; k < j; k++) {
                int operations = m[i][k] + m[k+1][j] + 
                               dims[i-1] * dims[k] * dims[j];
                
                if (operations < m[i][j]) {
                    m[i][j] = operations;
                    split[i][j] = k;
                    parenthesization[i][j] = "(" + parenthesization[i][k] + 
                                           parenthesization[k+1][j] + ")";
                }
            }
        }
    }
    
    return {m[1][n], parenthesization[1][n]};
}

int main() {
    ifstream inFile("matrix_input.txt");
    if (!inFile) {
        cerr << "Error opening input file" << endl;
        return 1;
    }

    ofstream outFile("matrix_results.csv");
    if (!outFile) {
        cerr << "Error opening output file" << endl;
        return 1;
    }

    outFile << "Number of Matrices,Dimensions,Method,Optimal Operations,"
            << "Parenthesization,Time (ms)" << endl;

    int n;
    while (inFile >> n) {  // n is the number of matrices
        vector<int> dimensions(n + 1);
        string dims_str;
        
        // Read dimensions
        for (int i = 0; i <= n; i++) {
            inFile >> dimensions[i];
            dims_str += to_string(dimensions[i]);
            if (i < n) dims_str += "x";
        }

        // Test recursive approach
        auto start = high_resolution_clock::now();
        MatrixChainResult recResult = matrixChainRecursive(dimensions, 1, n);
        auto end = high_resolution_clock::now();
        duration<double, milli> recDuration = end - start;
        
        outFile << n << "," << dims_str << ",Recursive," 
                << recResult.operations << ",\"" << recResult.parenthesization 
                << "\"," << fixed << setprecision(3) << recDuration.count() 
                << endl;

        // Test memoized approach
        start = high_resolution_clock::now();
        MatrixChainResult memResult = matrixChainMemoized(dimensions);
        end = high_resolution_clock::now();
        duration<double, milli> memDuration = end - start;
        
        outFile << n << "," << dims_str << ",Memoized," 
                << memResult.operations << ",\"" << memResult.parenthesization 
                << "\"," << fixed << setprecision(3) << memDuration.count() 
                << endl;

        // Test bottom-up approach
        start = high_resolution_clock::now();
        MatrixChainResult buResult = matrixChainBottomUp(dimensions);
        end = high_resolution_clock::now();
        duration<double, milli> buDuration = end - start;
        
        outFile << n << "," << dims_str << ",Bottom-up," 
                << buResult.operations << ",\"" << buResult.parenthesization 
                << "\"," << fixed << setprecision(3) << buDuration.count() 
                << endl;
    }

    inFile.close();
    outFile.close();
    
    cout << "Results have been written to matrix_results.csv" << endl;
    return 0;
}