#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>
#include <cstdlib>
#include <algorithm>
#include "number_data.h"
#include "Sorting_Algorithms.h"

using namespace std;
using namespace std::chrono;

// Function to write data to file
void write_data_to_file(const string& filename, const vector<int>& data) {
    ofstream file(filename);
    if (!file) {
        cerr << "Error opening file: " << filename << endl;
        return;
    }
    for (const int& num : data) {
        file << num << " ";
    }
    file.close();
}

// Function to read data from file
vector<int> read_data_from_file(const string& filename) {
    ifstream file(filename);
    vector<int> data;
    int num;
    while (file >> num) {
        data.push_back(num);
    }
    file.close();
    return data;
}

void generate_and_save_datasets() {
    const unsigned int sizes[] = {1000, 2000, 4000, 8000};
    
    for (unsigned int size : sizes) {
        int* random_data = new int[size];
        int* sorted_data = new int[size];
        int* reverse_data = new int[size];
        
        // Fill random, sorted, and reverse sorted arrays
        fill_random_numbers(random_data, size);
        fill_sorted_numbers(sorted_data, reverse_data, size);
        
        // Convert arrays to vectors
        vector<int> random_vec(random_data, random_data + size);
        vector<int> sorted_vec(sorted_data, sorted_data + size);
        vector<int> reverse_vec(reverse_data, reverse_data + size);
        
        // Write to files
        write_data_to_file("random_" + std::to_string(size) + ".txt", random_vec);
        write_data_to_file("sorted_" + std::to_string(size) + ".txt", sorted_vec);
        write_data_to_file("reverse_" + std::to_string(size) + ".txt", reverse_vec);
        
        delete[] random_data;
        delete[] sorted_data;
        delete[] reverse_data;
    }
}

int main() {
    // Step 1: Generate and save datasets
    generate_and_save_datasets();

    const unsigned int sizes[] = {1000, 2000, 4000, 8000};
    const string types[] = {"random", "sorted", "reverse"};

    for (const string& type : types) {
        for (unsigned int size : sizes) {
            // Read dataset from file
            vector<int> data = read_data_from_file(type + "_" + std::to_string(size) + ".txt");
            
            // Sort and measure
            double runtime;
            long long dataComparisons;
            
            // Bubble Sort
            vector<int> data_copy = data;
            dataComparisons = 0;
            Sorting_Algorithms::bubbleSort(data_copy, runtime, dataComparisons);
            cout << "Bubble Sort - " << type << " (" << size << " elements): Runtime = " << runtime << " ms, Comparisons = " << dataComparisons << endl;

            // Insertion Sort
            data_copy = data;
            dataComparisons = 0;
            Sorting_Algorithms::insertionSort(data_copy, runtime, dataComparisons);
            cout << "Insertion Sort - " << type << " (" << size << " elements): Runtime = " << runtime << " ms, Comparisons = " << dataComparisons << endl;

            // Quick Sort (Random Pivot)
            data_copy = data;
            dataComparisons = 0;
            Sorting_Algorithms::quickSortRandomized(data_copy, runtime, dataComparisons);
            cout << "Quick Sort (Random Pivot) - " << type << " (" << size << " elements): Runtime = " << runtime << " ms, Comparisons = " << dataComparisons << endl;

            // Quick Sort (Median-of-3)
            data_copy = data;
            dataComparisons = 0;
            Sorting_Algorithms::quickSortMedian(data_copy, runtime, dataComparisons);
            cout << "Quick Sort (Median-of-3) - " << type << " (" << size << " elements): Runtime = " << runtime << " ms, Comparisons = " << dataComparisons << endl;

            // Heap Sort
            data_copy = data;
            dataComparisons = 0;
            Sorting_Algorithms::heapSort(data_copy, runtime, dataComparisons);
            cout << "Heap Sort - " << type << " (" << size << " elements): Runtime = " << runtime << " ms, Comparisons = " << dataComparisons << endl;

            // 3-Way Merge Sort
            data_copy = data;
            dataComparisons = 0;
            Sorting_Algorithms::mergeSort3Way(data_copy, runtime, dataComparisons);
            cout << "3-Way Merge Sort - " << type << " (" << size << " elements): Runtime = " << runtime << " ms, Comparisons = " << dataComparisons << endl;
        }
    }
    
    return 0;
}
