# Running

## verify sorting
comment out the first main method in number_data.cpp, uncomment the second

compile with
g++ number_data.cpp -o project1.exe
./project1.exe

it will output confirmation that each sorting method does in fact sort, check both the main and the isSorted method to confirm it does this correctly

## running for results

compile with
g++ number_data.cpp -o project1.exe
./project1.exe > output.txt

running gives you:
the data processed in text files, these are generated for each run
a csv file with the sorting metics, time, and data comparisons
the output in a text file ||| use ./project.exe to simply run in console instead of piping to a text file
