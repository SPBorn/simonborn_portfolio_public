#include <iostream>
#include <vector>
#include <chrono>
#include <cstdlib>
#include <algorithm>
#include <ctime>


using namespace std;
using namespace std::chrono;

class Sorting_Algorithms {
public:
    // Bubble Sort (Optimized)
    static void bubbleSort(vector<int>& arr, double& runtime, long long& dataComparisons) {
        auto start = high_resolution_clock::now();
        bool swapped = true;
        int n = arr.size();
        int lastSwapped = n - 1;
        while (swapped) {
            swapped = false;
            int newLastSwapped = 0;
            for (int i = 0; i < lastSwapped; ++i) {
                dataComparisons++;
                if (arr[i] > arr[i + 1]) {
                    swap(arr[i], arr[i + 1]);
                    swapped = true;
                    newLastSwapped = i;
                }
            }
            lastSwapped = newLastSwapped;
        }
        auto stop = high_resolution_clock::now();
        runtime = duration<double, milli>(stop - start).count();
    }

    // Insertion Sort
    static void insertionSort(vector<int>& arr, double& runtime, long long& dataComparisons) {
        auto start = high_resolution_clock::now();
        int n = arr.size();
        for (int i = 1; i < n; ++i) {
            int key = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j] > key) {
                dataComparisons++;
                arr[j + 1] = arr[j];
                j--;
            }
            dataComparisons++;
            arr[j + 1] = key;
        }
        auto stop = high_resolution_clock::now();
        runtime = duration<double, milli>(stop - start).count();
    }

    // Quick Sort with Random Pivot
    static void quickSortRandomized(vector<int>& arr, double& runtime, long long& dataComparisons) {
        auto start = high_resolution_clock::now();
        quickSortRandom(arr, 0, arr.size() - 1, dataComparisons);
        auto stop = high_resolution_clock::now();
        runtime = duration<double, milli>(stop - start).count();
    }

    // Quick Sort with Median-of-3 Pivot
    static void quickSortMedian(vector<int>& arr, double& runtime, long long& dataComparisons) {
        auto start = high_resolution_clock::now();
        quickSortMedianOf3(arr, 0, arr.size() - 1, dataComparisons);
        auto stop = high_resolution_clock::now();
        runtime = duration<double, milli>(stop - start).count();
    }

    // Heap Sort
    static void heapSort(vector<int>& arr, double& runtime, long long& dataComparisons) {
        auto start = high_resolution_clock::now();
        int n = arr.size();
        for (int i = n / 2 - 1; i >= 0; --i) {
            heapify(arr, n, i, dataComparisons);
        }
        for (int i = n - 1; i > 0; --i) {
            swap(arr[0], arr[i]);
            heapify(arr, i, 0, dataComparisons);
        }
        auto stop = high_resolution_clock::now();
        runtime = duration<double, milli>(stop - start).count();
    }

    // 3-Way Merge Sort
    static void mergeSort3Way(vector<int>& arr, double& runtime, long long& dataComparisons) {
        auto start = high_resolution_clock::now();
        vector<int> temp(arr.size());
        mergeSort3(arr, 0, arr.size() - 1, temp, dataComparisons);
        auto stop = high_resolution_clock::now();
        runtime = duration<double, milli>(stop - start).count();
    }

private:
    // Helper functions for quick sort with random pivot
    static void quickSortRandom(vector<int>& arr, int low, int high, long long& dataComparisons) {
        if (low < high) {
            int pi = partitionRandom(arr, low, high, dataComparisons);
            quickSortRandom(arr, low, pi - 1, dataComparisons);
            quickSortRandom(arr, pi + 1, high, dataComparisons);
        }
    }

    static int partitionRandom(vector<int>& arr, int low, int high, long long& dataComparisons) {
        int randomIdx = low + rand() % (high - low + 1);
        swap(arr[randomIdx], arr[high]);
        return partition(arr, low, high, dataComparisons);
    }

    // Helper functions for quick sort with median-of-3 pivot
    static void quickSortMedianOf3(vector<int>& arr, int low, int high, long long& dataComparisons) {
        if (low < high) {
            int pi = partitionMedianOf3(arr, low, high, dataComparisons);
            quickSortMedianOf3(arr, low, pi - 1, dataComparisons);
            quickSortMedianOf3(arr, pi + 1, high, dataComparisons);
        }
    }

    static int partitionMedianOf3(vector<int>& arr, int low, int high, long long& dataComparisons) {
        int mid = (low + high) / 2;
        if (arr[mid] < arr[low]) swap(arr[low], arr[mid]);
        if (arr[high] < arr[low]) swap(arr[low], arr[high]);
        if (arr[high] < arr[mid]) swap(arr[mid], arr[high]);
        swap(arr[mid], arr[high - 1]);
        return partition(arr, low, high, dataComparisons);
    }

    // General partition function
    static int partition(vector<int>& arr, int low, int high, long long& dataComparisons) {
        int pivot = arr[high];
        int i = low - 1;
        for (int j = low; j < high; ++j) {
            dataComparisons++;
            if (arr[j] < pivot) {
                i++;
                swap(arr[i], arr[j]);
            }
        }
        swap(arr[i + 1], arr[high]);
        return i + 1;
    }

    // Heapify function for heap sort
    static void heapify(vector<int>& arr, int n, int i, long long& dataComparisons) {
        int largest = i;
        int left = 2 * i + 1;
        int right = 2 * i + 2;

        if (left < n && arr[left] > arr[largest]) {
            dataComparisons++;
            largest = left;
        }
        if (right < n && arr[right] > arr[largest]) {
            dataComparisons++;
            largest = right;
        }
        if (largest != i) {
            swap(arr[i], arr[largest]);
            heapify(arr, n, largest, dataComparisons);
        }
    }

    // 3-Way Merge Sort Helper Functions
    static void mergeSort3(vector<int>& arr, int low, int high, vector<int>& temp, long long& dataComparisons) {
        if (low >= high) return;

        int third = (high - low) / 3;
        int mid1 = low + third;
        int mid2 = high - third;

        mergeSort3(arr, low, mid1, temp, dataComparisons);
        mergeSort3(arr, mid1 + 1, mid2, temp, dataComparisons);
        mergeSort3(arr, mid2 + 1, high, temp, dataComparisons);

        merge3(arr, low, mid1, mid2, high, temp, dataComparisons);
    }

    static void merge3(vector<int>& arr, int low, int mid1, int mid2, int high, vector<int>& temp, long long& dataComparisons) {
        int i = low, j = mid1 + 1, k = mid2 + 1, l = low;
        while (i <= mid1 && j <= mid2 && k <= high) {
            dataComparisons++;
            if (arr[i] <= arr[j] && arr[i] <= arr[k]) temp[l++] = arr[i++];
            else if (arr[j] <= arr[i] && arr[j] <= arr[k]) temp[l++] = arr[j++];
            else temp[l++] = arr[k++];
        }
        while (i <= mid1 && j <= mid2) {
            dataComparisons++;
            temp[l++] = (arr[i] <= arr[j]) ? arr[i++] : arr[j++];
        }
        while (j <= mid2 && k <= high) {
            dataComparisons++;
            temp[l++] = (arr[j] <= arr[k]) ? arr[j++] : arr[k++];
        }
        while (i <= mid1 && k <= high) {
            dataComparisons++;
            temp[l++] = (arr[i] <= arr[k]) ? arr[i++] : arr[k++];
        }
        while (i <= mid1) temp[l++] = arr[i++];
        while (j <= mid2) temp[l++] = arr[j++];
        while (k <= high) temp[l++] = arr[k++];
        for (int m = low; m <= high; ++m) arr[m] = temp[m];
    }
};