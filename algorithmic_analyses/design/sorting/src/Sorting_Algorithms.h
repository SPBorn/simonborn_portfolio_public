#pragma once
#include <iostream>
#include <vector>
#include <chrono>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include "Sorting_Algorithms.cpp"	

using namespace std;
using namespace std::chrono;

class Sorting_Algorithms {
public:
    // Bubble Sort (Optimized)
    static void bubbleSort(vector<int>& arr, double& runtime, long long& dataComparisons);

    // Insertion Sort
    static void insertionSort(vector<int>& arr, double& runtime, long long& dataComparisons);

    // Quick Sort with Random Pivot
    static void quickSortRandomized(vector<int>& arr, double& runtime, long long& dataComparisons);

    // Quick Sort with Median-of-3 Pivot
    static void quickSortMedian(vector<int>& arr, double& runtime, long long& dataComparisons);

    // Heap Sort
    static void heapSort(vector<int>& arr, double& runtime, long long& dataComparisons);

    // 3-Way Merge Sort
    static void mergeSort3Way(vector<int>& arr, double& runtime, long long& dataComparisons);

private:
    // Helper functions for quick sort with random pivot
    static void quickSortRandom(vector<int>& arr, int low, int high, long long& dataComparisons);
    static int partitionRandom(vector<int>& arr, int low, int high, long long& dataComparisons);

    // Helper functions for quick sort with median-of-3 pivot
    static void quickSortMedianOf3(vector<int>& arr, int low, int high, long long& dataComparisons);
    static int partitionMedianOf3(vector<int>& arr, int low, int high, long long& dataComparisons);

    // General partition function
    static int partition(vector<int>& arr, int low, int high, long long& dataComparisons);

    // Heapify function for heap sort
    static void heapify(vector<int>& arr, int n, int i, long long& dataComparisons);

    // 3-Way Merge Sort Helper Functions
    static void mergeSort3(vector<int>& arr, int low, int high, vector<int>& temp, long long& dataComparisons);
    static void merge3(vector<int>& arr, int low, int mid1, int mid2, int high, vector<int>& temp, long long& dataComparisons);
};
