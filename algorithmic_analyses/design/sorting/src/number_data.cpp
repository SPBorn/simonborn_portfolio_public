// random_numbers.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>
#include <cstdlib>
#include <algorithm>
#include "number_data.h"
#include "Sorting_Algorithms.cpp"

using namespace std;
using namespace std::chrono;

void fill_random_numbers (int arr [], const unsigned int SIZE)
{
	// Pre:  SIZE is no more than a million, arr has been declared to be
	//       an array of at least SIZE elements.
	// Post: The first SIZE elements of the array arr have been populated with
	//       random integers between 0 and one million.

	for (int i = 0; i < SIZE; i++)
	{
		arr[i] = rand() % 1000001;
	}
}

void fill_sorted_numbers (int asc [], int dsc [], const unsigned int SIZE)
{
	// Pre:  SIZE is no more than a million, and asc and dsc have been declared
	//       to be arrays of at least SIZE elements.
	// Post: The first SIZE elements of the array asc have been populated with
	//       integers between 0 and one million in ascending order, and dsc has
	//       the same numbers in descending order.

	int step = 1000000 / SIZE;

	for (int i = 0; i < SIZE; i++)
	{
		asc[i] = dsc [SIZE-1-i] = step*i;
	}
}

/*
Example for usage:

const unsigned int SIZE = 8000;

int main()
{
	int numbers [SIZE];
	int increasing [SIZE];
	int decreasing [SIZE];

	fill_random_numbers (numbers, SIZE);
	fill_sorted_numbers (increasing, decreasing, SIZE);

	// NOW apply sorting algorithms to a copy of each array or copy of a slice of the array.

}
*/

/*
***********************************************************************************************************
*/


// Function to write data to file
void write_data_to_file(const string& filename, const vector<int>& data) {
    ofstream file(filename);
    if (!file) {
        cerr << "Error opening file: " << filename << endl;
        return;
    }
    for (const int& num : data) {
        file << num << " ";
    }
    file.close();
}

// Function to read data from file
vector<int> read_data_from_file(const string& filename) {
    ifstream file(filename);
    vector<int> data;
    int num;
    while (file >> num) {
        data.push_back(num);
    }
    file.close();
    return data;
}

void generate_and_save_datasets() {
    const unsigned int sizes[] = {1000, 2000, 4000, 8000};
    
    for (unsigned int size : sizes) {
        int* random_data = new int[size];
        int* sorted_data = new int[size];
        int* reverse_data = new int[size];
        
        // Fill random, sorted, and reverse sorted arrays
        fill_random_numbers(random_data, size);
        fill_sorted_numbers(sorted_data, reverse_data, size);
        
        // Convert arrays to vectors
        vector<int> random_vec(random_data, random_data + size);
        vector<int> sorted_vec(sorted_data, sorted_data + size);
        vector<int> reverse_vec(reverse_data, reverse_data + size);
        
        // Write to files
        write_data_to_file("random_" + std::to_string(size) + ".txt", random_vec);
        write_data_to_file("sorted_" + std::to_string(size) + ".txt", sorted_vec);
        write_data_to_file("reverse_" + std::to_string(size) + ".txt", reverse_vec);
        
        delete[] random_data;
        delete[] sorted_data;
        delete[] reverse_data;
    }
}

// Function to write results to CSV file
void write_result_to_csv(ofstream& csvFile, const string& algorithm, const string& type, unsigned int size, double runtime, long long comparisons) {
    csvFile << algorithm << "," << type << "," << size << "," << runtime << "," << comparisons << endl;
}

/*
used to check the validity of the sorting algorithms
*/
bool isSorted(const vector<int>& arr) {
    for (size_t i = 1; i < arr.size(); ++i) {
        if (arr[i - 1] > arr[i]) {
            return false;
        }
    }
    return true;
}

/*
The main function for the project
it has both quick sorts implimented
to test the sorting algorithms for validity see the below code under this main
*/
int main() {
    // Step 1: Generate and save datasets
    generate_and_save_datasets();

    const unsigned int sizes[] = {1000, 2000, 4000, 8000};
    const string types[] = {"random", "sorted", "reverse"};

	ofstream csvFile("sorting_results.csv");
	csvFile << "Algorithm,Data_Type,Size,Runtime(ms),Comparisons" << endl;

    for (const string& type : types) {
        for (unsigned int size : sizes) {
            // Read dataset from file
            vector<int> data = read_data_from_file(type + "_" + std::to_string(size) + ".txt");
            
            // Sort and measure
            double runtime;
            long long dataComparisons;
            
            // Bubble Sort
            vector<int> data_copy = data;
            dataComparisons = 0;
            Sorting_Algorithms::bubbleSort(data_copy, runtime, dataComparisons);
            cout << "Bubble Sort - " << type << " (" << size << " elements): Runtime = " << runtime << " ms, Comparisons = " << dataComparisons << endl;
			write_result_to_csv(csvFile, "Bubble Sort", type, size, runtime, dataComparisons);

            // Insertion Sort
            data_copy = data;
            dataComparisons = 0;
            Sorting_Algorithms::insertionSort(data_copy, runtime, dataComparisons);
            cout << "Insertion Sort - " << type << " (" << size << " elements): Runtime = " << runtime << " ms, Comparisons = " << dataComparisons << endl;
			write_result_to_csv(csvFile, "Insertion Sort", type, size, runtime, dataComparisons);

            // Quick Sort (Random Pivot)
            data_copy = data;
            dataComparisons = 0;
            Sorting_Algorithms::quickSortRandomized(data_copy, runtime, dataComparisons);
            cout << "Quick Sort (Random Pivot) - " << type << " (" << size << " elements): Runtime = " << runtime << " ms, Comparisons = " << dataComparisons << endl;
			write_result_to_csv(csvFile, "Quick Sort (Random Pivot)", type, size, runtime, dataComparisons);

            // Quick Sort (Median-of-3)
            data_copy = data;
            dataComparisons = 0;
            Sorting_Algorithms::quickSortMedian(data_copy, runtime, dataComparisons);
            cout << "Quick Sort (Median-of-3) - " << type << " (" << size << " elements): Runtime = " << runtime << " ms, Comparisons = " << dataComparisons << endl;
			write_result_to_csv(csvFile, "Quick Sort (Median-of-3)", type, size, runtime, dataComparisons);

            // Heap Sort
            data_copy = data;
            dataComparisons = 0;
            Sorting_Algorithms::heapSort(data_copy, runtime, dataComparisons);
            cout << "Heap Sort - " << type << " (" << size << " elements): Runtime = " << runtime << " ms, Comparisons = " << dataComparisons << endl;
			write_result_to_csv(csvFile, "Heap Sort", type, size, runtime, dataComparisons);

            // 3-Way Merge Sort
            data_copy = data;
            dataComparisons = 0;
            Sorting_Algorithms::mergeSort3Way(data_copy, runtime, dataComparisons);
            cout << "3-Way Merge Sort - " << type << " (" << size << " elements): Runtime = " << runtime << " ms, Comparisons = " << dataComparisons << endl;
			write_result_to_csv(csvFile, "3-Way Merge Sort", type, size, runtime, dataComparisons);
        }
    }
    
    return 0;
}

/*
the following main can be used to test the sorting algorithms:
upon being challenged a run of this code after checking to see what it does should provide
plenty of evidence to support the correctness of the sorting algorithms


int main() {
    vector<int> data = {5, 2, 9, 1, 5, 6, 3, 8, 4, 7};
	double runtime;
	long long dataComparisons;

	isSorted(data) ? cout << "Original Data: Sorted\n" : cout << "Original Data: Not Sorted\n";
	
	// Bubble Sort
	vector<int> data_copy = data;
	dataComparisons = 0;
	Sorting_Algorithms::bubbleSort(data_copy, runtime, dataComparisons);
	isSorted(data_copy) ? cout << "Bubble Sort: Sorted\n" : cout << "Bubble Sort: Not Sorted\n";

	// Insertion Sort
	data_copy = data;
	dataComparisons = 0;
	Sorting_Algorithms::insertionSort(data_copy, runtime, dataComparisons);
	isSorted(data_copy) ? cout << "Insertion Sort: Sorted\n" : cout << "Insertion Sort: Not Sorted\n";

	// Quick Sort (Random Pivot)
	data_copy = data;
	dataComparisons = 0;
	Sorting_Algorithms::quickSortRandomized(data_copy, runtime, dataComparisons);
	isSorted(data_copy) ? cout << "Quick Sort (Random Pivot): Sorted\n" : cout << "Quick Sort (Random Pivot): Not Sorted\n";

	// Quick Sort (Median-of-3)
	data_copy = data;
	dataComparisons = 0;
	Sorting_Algorithms::quickSortMedian(data_copy, runtime, dataComparisons);
	isSorted(data_copy) ? cout << "Quick Sort (Median-of-3): Sorted\n" : cout << "Quick Sort (Median-of-3): Not Sorted\n";

	// Heap Sort
	data_copy = data;
	dataComparisons = 0;
	Sorting_Algorithms::heapSort(data_copy, runtime, dataComparisons);
	isSorted(data_copy) ? cout << "Heap Sort: Sorted\n" : cout << "Heap Sort: Not Sorted\n";

	// 3-Way Merge Sort
	data_copy = data;
	dataComparisons = 0;
	Sorting_Algorithms::mergeSort3Way(data_copy, runtime, dataComparisons);
	isSorted(data_copy) ? cout << "3-Way Merge Sort: Sorted\n" : cout << "3-Way Merge Sort: Not Sorted\n";


    return 0;
}
*/
