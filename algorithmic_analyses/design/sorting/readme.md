# Comparative Analysis of Sorting Algorithms

## Overview
This document presents a detailed analysis of six sorting algorithms, examining both their theoretical foundations and practical performance across different input scenarios. We investigate bubble sort, insertion sort, heap sort, three-way merge sort, and two variations of quick sort (using median-of-three and random pivot selection).

## Methodology
Our analysis compares theoretical time complexities with actual runtime measurements across three distinct input scenarios:
- Sorted arrays (best case)
- Reverse-sorted arrays (typically worst case)
- Random arrays (average case)

All measurements were conducted with input sizes ranging from 1,000 to 8,000 elements.

## Algorithm Analysis

### Bubble Sort
**Theoretical Complexity:**
- Worst/Average Case: Θ(n²)
- Best Case: Θ(n)

**Practical Performance:**
- Reverse-sorted data (n=1000 to 2000): Runtime increases from 5ms to 25ms
- Sorted data (n=1000 to 2000): Runtime increases from 0.41ms to 0.81ms
- Demonstrates clear quadratic growth in non-sorted scenarios

**Key Insight:** Performance heavily depends on input order, showing dramatic efficiency differences between sorted and unsorted data.

### Insertion Sort
**Theoretical Complexity:**
- Worst/Average Case: Θ(n²)
- Best Case: Θ(n)

**Practical Performance:**
- Random data (n=1000): 1.45ms
- Random data (n=8000): 94.64ms
- Sorted data (n=1000): 0.0066ms
- Sorted data (n=8000): 0.0774ms
- Reverse-sorted data (n=8000): 187.54ms

**Key Insight:** Exceptionally efficient for nearly-sorted data but struggles significantly with reverse-sorted arrays.

### Quick Sort (Random Pivot)
**Theoretical Complexity:**
- Average Case: Θ(n log n)
- Worst Case: Θ(n²)

**Practical Performance:**
- Random data (n=1000): 0.2255ms
- Random data (n=8000): 1.4625ms
- Sorted data (n=8000): 1.5277ms
- Reverse-sorted data (n=8000): 1.1995ms

**Key Insight:** Random pivot selection provides consistent performance across different input patterns, showing minimal dependency on input order.

### Quick Sort (Median-of-Three)
**Theoretical Complexity:**
- Average Case: Θ(n log n)
- Worst Case: Θ(n²)

**Practical Performance:**
- Random data (n=1000): 0.2471ms
- Random data (n=8000): 1.9053ms
- Sorted data (n=8000): 2.6602ms
- Reverse-sorted data (n=8000): 1.9053ms

**Key Insight:** More reliable than random pivot selection, though performance depends more on pivot selection strategy than input pattern.

### Heap Sort
**Theoretical Complexity:**
- All Cases: Θ(n log n)

**Practical Performance:**
- Random data (n=1000): 0.2905ms
- Random data (n=8000): 2.2455ms
- Sorted data (n=8000): 2.4399ms
- Reverse-sorted data (n=8000): 1.9479ms

**Key Insight:** Provides consistent performance regardless of input pattern, making it reliable for unknown data distributions.

### Three-Way Merge Sort
**Theoretical Complexity:**
- All Cases: Θ(n log n)

**Practical Performance:**
- Random data (n=1000): 0.1932ms
- Random data (n=8000): 1.5912ms
- Sorted data (n=8000): 1.0483ms
- Reverse-sorted data (n=8000): 0.8922ms

**Key Insight:** Shows remarkable consistency across different input patterns and demonstrates excellent efficiency with larger datasets.

## Comparative Analysis

Looking at the performance across all algorithms, several patterns emerge:

1. **Input Sensitivity:** Simple algorithms (bubble, insertion) show dramatic performance variations based on input patterns, while more sophisticated algorithms maintain more consistent performance.

2. **Scalability:** The n log n algorithms (heap sort, merge sort, quick sort) clearly outperform quadratic algorithms as input size grows.

3. **Practical Considerations:**
   - Three-way merge sort shows the most consistent performance across all input patterns
   - Quick sort variants demonstrate how pivot selection strategies can significantly impact performance
   - Heap sort offers a good balance of consistent performance and efficiency

## Conclusion

Our analysis confirms that theoretical complexities largely predict practical performance, though with some nuances. For general-purpose sorting:
- Three-way merge sort and heap sort offer the most reliable performance
- Quick sort with median-of-three provides excellent average-case performance
- Simpler algorithms like bubble and insertion sort should be reserved for small or nearly-sorted datasets

The choice of algorithm should consider both the expected input patterns and the specific requirements of the application, such as space constraints or stability requirements.
